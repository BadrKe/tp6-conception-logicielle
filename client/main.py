import requests
from dotenv import load_dotenv
import os


def get_mots():
    r = requests.get(URL + '/mots')
    print(r.json())


def add_mot(id: str, caracteres: str):
    r = requests.post(URL + '/mot',
                      data={'id': id, 'caracteres': caracteres})
    r.text


if __name__ == "main":
    load_dotenv()
