# TP6-conception-logicielle

## Pour lancer l'application

Il faut d'abord installer les prérequis

```
git clone https://gitlab.com/BadrKe/tp5-conception-logicielle.git
cd tp6-conception-logicielle
"
```
Puis on peut lancer le serveur

```
cd server
pip install -r "requirements.txt
python3 main.py
```

Ou lancer le client

```
cd client
pip install -r "requirements.txt
python3 main.py
```