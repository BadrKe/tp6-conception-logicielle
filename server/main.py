from fastapi import FastAPI
from mot import Mot

app = FastAPI()


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/mots")
def get_all_words():
    return["arbre", "maison", "voiture"]


@app.post("/mot")
async def post_word(mot: Mot):
    print(mot.caracteres)
